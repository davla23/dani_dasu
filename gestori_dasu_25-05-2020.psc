Algoritmo gestoria_DASU
	
	// Declaraci�n de variables, arrays
	
	salir<- falso;

	definir posicion como real;
	dimension posicion[8];
	
	definir i como entero;
	
	definir documentos como cadena;
	dimension documentos[8];	
	
	definir opcion_elegida como caracter;
	
	final Es Entero;
	termina es entero;
	textoo es cadena;
	
	definir encontrado como logico;
	encontrado=falso;//si hay un documento o no.
	
	
	
	Escribir "-----------------------------------------------------------"
	Escribir " Gestor�a DASU - Gesti�n Documental de la Gestor�a" 
	EScribir "-----------------------------------------------------------"
	
	// *** Rellenamos de contenido los arrays **********
	
	// Rellenamos el Primer array posicion
	
	Para i<-1 Hasta 8 Con Paso 1 Hacer
		Escribir ("Introduzca El Numero del documento")
		Leer posicion[i]
	Fin Para
	
	// Visualizmos el contenido metido en el array posicion
	
	Para i<-1 Hasta 8 Con Paso 1 Hacer
		Escribir posicion[i],"  ",sin saltar;
	FinPara
	Escribir ""
	
	// Rellenamos de datos Segundo array documentos.
	Para i<-1 Hasta 8 Con Paso 1 Hacer
		Escribir ("Introduzca los datos del documento")
		Leer documentos[i]
	Fin Para
	
	// Visualizmos el contenido metido en el array posicion
	
	Para i<-1 hasta 8 con paso 1 hacer
		Escribir documentos[i],"  " sin saltar;
	FinPara
	Escribir ""
	
	
	// ***** Fin del rellenado de datos del array ***************************************
	
// *******Aqui haremos el Men�, con las opciones que va hacer el programa *************
// Procedemos a realizar un bucle, mientras, para que nos repita el menu 
// hasta que se presione s, o S, no saldra de programa.
	Repetir
		//mostrar menu
		//Borrar Pantalla
		Escribir "GESTION DE DOCUMENTOS"
		Escribir "1. VISUALIZACI�N DE DOCUMENTOS"
		Escribir "2. B�SQUEDA DE DOCUMENTOS"
		Escribir "3. EDICI�N DE DOCUMENTOS"
		Escribir "4. CREAR DOCUMENTOS"
		Escribir "5. BORRAR DOCUMENTOS"
		Escribir "6. IMPRESI�N DE DOCUMENTOS"
		//ingresar una opcion
		Escribir "Elige una opci�n 1-6 (pulsa la letra S para salir)"
		Leer eleccion
		//procesar esa opci�n
		Segun eleccion hacer
			"1":	
				Escribir "VISUALIZACI�N DE DOCUMENTOS"
				visualizar(documentos,posicion)
				
				Escribir " "
				Escribir "Realizado, pulsa una tecla para continuar"
				leer espera;
			"2":
				Escribir "B�SQUEDA DE DOCUMENTOS"
				busqueda()
				
				Escribir " "
				Escribir "Realizado, pulsa una tecla para continuar"
				leer espera;
			"3":
				Escribir "EDICI�N DE DOCUMENTOS"
				edita(documentos,posicion)
				Escribir " "
				Escribir "Realizado, pulsa una tecla para continuar"
				leer espera;
			"4":Escribir "CREAR NUEVO DOCUMENTO"
				crear(posicion,documentos,final,i,textoo)
				leer espera;
			"5":
				Escribir "BORRAR DOCUMENTOS"
				borrado(posicion,documentos,termina)
				Escribir " "
				Escribir "Realizado, pulsa una tecla para continuar"
				leer espera;
			"6" :
				Escribir "IMPRESION DE DOCUMENTOS"
				imprim()
				Escribir " "
				Escribir "Realizado, pulsa una tecla para continuar"
				leer espera;
				
				
				
		FinSegun		
	Hasta Que eleccion="s" o eleccion="S"
	Escribir "FIN"
FinAlgoritmo


//************* Aqui finaliza el menu del programa ***************************


//************************************************************"

//****** Aqui crearemos el proceso visualizacion de documentos  **************
Subproceso visualizar (d Por Referencia, p Por Referencia )
	
	Para i<-1 Hasta 8 Con Paso 1 Hacer
	
		Escribir "La posicion: ",i," Tiene el documento ",p[i], " y su contenido es ",d[i];
	
Fin Para
	
FinSubProceso


// **** Aqui comienza el proceso busqueda de documentos ************

SubProceso busqueda()
	EScribir "Indice"
FinSubProceso

// **** Aqui finaliza el proceso busqueda de documentos ************


// **** Aqui comienza la edicion de documentos ************
SubProceso edita(d Por Referencia, p Por Referencia)
	
	docu es entero;
	palabra es cadena;
	
	
	Escribir "Indique el numero del documento que quiere modicar"
	Leer docu;
	
	Escribir "Introduzca el texto que quiere introducir en el documento"
	Leer palabra;
	
	Para i<-1 Hasta 8 Con Paso 1 Hacer
		Si p[i]=docu Entonces
			d[i]=Concatenar(d[docu],palabra)
			Escribir "El documento editado es ",d[i];		
		Fin Si
	Fin Para
	
FinSubProceso

// **** Aqui finaliza la edicion de documentos ************


// **** Aqui comienza la creacion de documentos ************

SubProceso crear(p por Referencia,d por referencia,final por referencia,i por referencia,textoo por referencia)
		final<-8;
		Escribir "Introduce el texto que quieres a�adir:"
		Leer textoo;
		d[final]<-textoo;
		Escribir "El texto se ha a�adido a la posicion ",final;
	
FinSubProceso
// **** Aqui finaliza la creacion de documentos ************


// **** Aqui comienza el borrado de documentos ************
SubProceso borrado(posicion Por Referencia,d Por Referencia,termina Por Referencia)
	termina<-6;
	
	Si documento_lleno(d) Entonces
		d[termina]<-"$";
		
		Escribir "El contenido en la posicion  ",termina,"  esta eliminado"
		termina=termina-1;
	finsi
	
FinSubProceso
// **** Aqui finaliza el borrado de documentos ************
// **** Aqui se comprueba si el documento esta lleno o vacio, el subproceso "borrado" *****
//llama a esta funci�n para comprobar si esta lleno o vacio el documento.*******

Funcion lleno <- documento_lleno(d por referencia)
	lleno es logico;
	lleno<-falso;
	
	Para i<-1 Hasta 8 Hacer
		Si d[i]!="$" Entonces
			
			lleno<-verdadero
		SiNo
			lleno<-falso
		Fin Si
	FinPara
	Si lleno es verdadero Entonces
		Escribir ' Hay documento en esta posicion';
	SiNo
		Escribir 'No hay documento en esta posicion';
	Fin Si
Fin Funcion
// **** Aqui termina la funci�n, si el documento esta lleno o vacio

SubProceso imprim()
FinSubProceso
	